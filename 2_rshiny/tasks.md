1. First, we install a conda environment including R, R-Shiny, some more packages and postgreseql. Name of the environment is **rshiny**.

```bash
cd $HOME/3rd_denbi_user_meeting__service_deployment/2_rshiny
conda env create --file environment.yaml
conda activate rshiny
```

2. Next, we initialize a database. We will use a local postgresql instance for testing our shiny app.
Thereby, our single cell data will be stored in a database named **sc_data**.
We also create a database user and assign a password (our default is *password*).

```bash
initdb -D local_db
pg_ctl -D local_db -l logfile start

createuser --pwprompt my_user
createdb --owner=my_user sc_data
```

3. Execution of **init_db.R** script will create required data tables and store data in our database.
Before execution, we need to set a few environmental variables.

```bash
export DB_USER="my_user"
export DB_PASSWORD="password"
export DB_NAME="sc_data"
export DB_HOST="localhost"
export DB_PORT="5432"
export PROJECT_WORKSPACE=$HOME"/3rd_denbi_user_meeting__service_deployment"

Rscript ../scripts/init_db.R
```

4. Lets check if data is inserted into the DB:

- Start R with `R` and copy the following script:

```R
library("DBI")
library("tidyverse")
library("dbplyr")

get_connection <- function() {
    con <- dbConnect(
        RPostgreSQL::PostgreSQL(),
        dbname = Sys.getenv("DB_NAME"),
        host=Sys.getenv("DB_HOST"),
        port=Sys.getenv("DB_PORT"),
        user=Sys.getenv("DB_USER"),
        password=Sys.getenv("DB_PASSWORD"))
    return(con)
}

con <- get_connection()
meta <- tbl(con, "meta")
meta %>%
  head()
meta %>%
  collect() %>%
  dim()
counts <- tbl(con, "counts")
counts %>%
  head()
counts %>%
  summarise(n=n())
dbDisconnect(con)
```
- you can get out of R with `quit()`

5. Finally, we can start the R-shiny app

```bash
export PROJECT_WORKSPACE=$HOME"/3rd_denbi_user_meeting__service_deployment"
export SHINY_APP=$PROJECT_WORKSPACE"/scripts"
export SHINY_PORT="8000"
Rscript ../scripts/start_shiny.R
```

6. We stop our database using a kill command and therefore we need its pid - its a process which was started with .../envs/rshiny/bin/postgres -D local_db

```bash
ps aux | grep postgres
kill #add pid here
```