## shinyloadtest

The shinyloadtest package and the accompanying shinycannon software enable load testing deployed Shiny applications.

Load testing helps developers and administrators estimate how many users their application can support. If an application requires tuning, load testing and load test result analysis can be used to identify performance bottlenecks and to guide changes to infrastructure, configuration, or code.

https://rstudio.github.io/shinyloadtest/

### First Step:  Collect a test session in R

```R
options(browser="/usr/bin/google-chrome-stable")
shinyloadtest::record_session('http://denbi-training.bihealth.org/') // Record session in browser and close window to stop recording
```

### Second Step: Use shinycannon for stresstest


wget shinyloadtest::shinyloadtest_report(df, "run1.html") 
java -jar shinycannon-1.0.0-9b22a92.jar [RECORDING-PATH] [APP-URL]
java -jar shinycannon-1.0.0-9b22a92.jar recording.log $URL --workers $howManyConcurrentWorkers --loaded-duration-minutes 2 --output-dir $dirLocation

### Third Step: Analyze date in R


df <- shinyloadtest::load_runs("$howManyConcurrentWorkers" = "./dirLocation")
shinyloadtest::shinyloadtest_report(df, "Report.html")
