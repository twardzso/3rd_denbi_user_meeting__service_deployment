## traefik

1. Adjust ssh config 

    ```bash
    Host shiny_demo_manager # first vm
      HostName 172.16.XXX.XXX
      IdentityFile PATH_TO_KEY
      User ubuntu
      ProxyJump denbi-jumphost-01.bihealth.org
      LocalForward 8181 127.0.0.1:8181 # this is for theia-ide
      DynamicForward 127.0.0.1:5000 # this opens a socks proxy on port 5000
    ```

2. Remove stack 

    ```bash
    sudo docker stack rm myapp
    ```
3. Add traefik container to service.yaml:
    
    ```yaml
    image: traefik:latest
    depends_on:
      - sc_explorer
    restart: always
    ports:
      - "8080:8080" # traefik dashboard
      - "8282:8282" # shiny app
    command:
      - --log.level=DEBUG
      - --api.dashboard=true
      - --providers.docker=true
      - --providers.docker.swarmMode=true
      - --providers.docker.exposedbydefault=false
      - --providers.docker.network=net
      - --entrypoints.web.address=:8282
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    networks:
      - net
    deploy:
      labels:
      - --traefik.enable=true
      - --traefik.http.routers.api.rule=Host(`FloatingIPManagerVM`)
      - --traefik.http.routers.api.service=api@internal
      placement:
        constraints: [ node.role == manager ]
    ```
   
4. Adjust shiny-app container:

    ```yaml
      sc_explorer:
        image: rshiny:v1
        environment:
          - "DB_USER=my_user"
          - "DB_PASSWORD=password"
          - "DB_NAME=sc_data"
          - "DB_HOST=myapp_db"
          - "DB_PORT=5432"
          - "SHINY_APP=/scripts/app.R"
          - "SHINY_PORT=8000"
        volumes:
          - "${WORKSPACE}/3rd_denbi_user_meeting__service_deployment/scripts:/scripts"
        networks:
          - net
        deploy:
          replicas: 1
          labels:
            - "traefik.enable=true"
            - "traefik.http.routers.sc_explorer.rule=Host(`FloatingIPManagerIP`)"
            - "traefik.http.services.sc_explorer.loadbalancer.server.port=8000"
            - "traefik.http.routers.sc_explorer.entrypoints=web"
        command: Rscript /scripts/start_shiny.R
    ```
   
5. Deploy docker stack:

    ```bash
    sudo /bin/bash start_services.sh
    ```
   
6. Check deployment of shiny app with traefik on your manager vm with:

    ```bash
    curl yourFloatingIPManagerVM:8282
    ```

7. Check again under local browser:

- configure your browser to use a socks proxy e.g.:
    
    `"socks proxy" --> localhost:8282`
- your URL will be:
    
    `yourFloatingIPManagerVM:8282`
    
 - your Dashboard will be:
    
    `yourFloatingIPManagerVM:8080`

