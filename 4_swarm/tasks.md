
## create swarm

1. Lets create a swarm by its initialization on our manager node.

```bash
sudo docker swarm init
```

2. Next, we create a worker node:
- Start a new VM in the dashboard
- Add worker to ssh-config and ssh into worker node
- Install docker on worker node (task 1)
- Clone git repo (task 1) 
- Create custom r-shiny image (task 3)

3. To attach another node to the swarm we need the previous join-token:

```
sudo docker swarm $JOIN_TOKEN worker
```

4. Back on our manager node, we can view all swarm nodes by typing:

```bash
sudo docker node ls
```

# stack

5. Once we have a swarm, we can deploy our services to the swarm. This will create a docker stack called myapp.

```bash
cd $HOME/3rd_denbi_user_meeting__service_deployment/4_swarm
sudo /bin/bash start_services.sh
```

5. You can list active stacks and deployed services by:

```bash
sudo docker stack ls
sudo docker service ls
```

Debugging:
```bash
sudo docker service logs $UUIDofService 
journalctl -u docker.service | tail -n 50 
```

6. Deployed services can be updated while running; e.g. we can easily increase the number of replicas to 5:

```bash
sudo docker service update myapp_sc_explorer --replicas=5
```

7. Finally, we remove our stack:

```bash
sudo docker stack rm myapp
```