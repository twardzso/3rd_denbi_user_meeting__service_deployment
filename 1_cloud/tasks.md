# Initialization

## 0 Prerequisites

- You need to have an elixir account to become a member of our training project at the de.NBI cloud in Berlin
- Your public ssh must be added at the de.NBI portal: https://cloud.denbi.de/portal/ (or at the elixir portal) to able to connect to the jumphost

## 1 Deploy a vm in training project

1. Login  with elixir account at de.NBI cloud Berlin horizon dashboard (OpenStack graphical interface): https://denbi-cloud.bihealth.org/
2. First time users need to add their public ssh key to the horizon dashboard, to be able to inject your public key when you create a new vm:
_Project --> Compute --> Key Pairs --> Import Public Key_

3. Create a Theia virtual machine: _Project --> Compute --> Instances --> Launch Instance_

    - Details: set an **Instance Name with your zoomname** in it
    - Source: set **Create new volume** to **no** and choose "theia" image
    - Flavor:  **de.NBI default**
    - Networks: select **onlineTraining-network**
    - Key pair: choose **YOURKEY**
    - Launch instance

4. Assign a floating ip to get ssh access: _Project --> Compute --> Instances --> choose your instance --> assing floating vm
 
    - Assign floating IP from range 172.16.102.x or172.16.103.x (please look into excel sheet to see which one you should use)

    Please go sure you dont accidently take a floating ip adresse from another ip range. They are reserved for another use case and will not work with you vm.

## 2 Connect to instance via ssh

Connection to OpenStack vm at de.NBI cloud site Berlin is only possible via jumphost. Therefore you have two options:

**A)** Setup ssh-config under .ssh/config (recommended)

  - Windows 10:

```bash
Host denbi-jumphost-01.bihealth.org
    HostName denbi-jumphost-01.bihealth.org
    User USERNAME
    IdentityFile PATH_TO_KEY


Host shiny_demo_manager  # first vm
  HostName 172.16.XXX.XXX
  IdentityFile PATH_TO_KEY
  User ubuntu
  ProxyCommand C:\Windows\System32\OpenSSH\ssh.exe denbi-jumphost-01.bihealth.org -W %h:%p
  LocalForward 8000 localhost:8000
  LocalForward 8181 localhost:8181
```

  - Linux:

```bash
Host denbi-jumphost-01.bihealth.org
    HostName denbi-jumphost-01.bihealth.org
    User USERNAME
    IdentityFile PATH_TO_KEY
    ServerAliveInterval 120


Host shiny_demo_manager # first vm
  HostName 172.16.XXX.XXX
  IdentityFile PATH_TO_KEY
  User ubuntu
  ProxyJump denbi-jumphost-01.bihealth.org
  LocalForward 8181 localhost:8181 # other option is to use ssh -L 8181:localhost:8181 ubuntu@shiny_demo_manager
  LocalForward 8000 localhost:8000
```

**B)** Manually jump from your client to jumphost and from there further to your vm with [ssh-agent-forwarding](https://www.ssh.com/ssh/agent)

1. Start ssh-agent:

    ```bash
    eval `ssh-agent` // eval $(ssh-agent)
    ```

2. Add ssh private key:

    ```bash
    ssh-add .ssh/id_rsa
   ```

    show identies:

    ```bash
    ssh-add -l
   ```

3. Connect at first from your client to jumphost: 

    ```bash
    ssh -A yourElixirAccountName@denbi-jumphost-01.bihealth.org
    ```

    And from the jumphost you can connect further to the floating ip of your vm

    ```bash
    ssh ubuntu@yourFloatingIpOfVM
    ```
   
### 3. Setup project

1. Clone repository

```bash
cd $HOME
git clone https://gitlab.com/twardzso/3rd_denbi_user_meeting__service_deployment.git
```

2. Download and install conda

```bash
wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh
/bin/bash Miniconda3-latest-Linux-x86_64.sh
# type: yes, (empty enter), yes
source ~/.bashrc
```

3. Install docker

```bash
sudo apt-get update
sudo apt-get install -y docker.io
sudo echo '{"mtu": 1450}' | sudo tee -a /etc/docker/daemon.json
sudo service docker restart
sudo usermod -aG docker ${USER}
sudo systemctl restart theia-ide
```
