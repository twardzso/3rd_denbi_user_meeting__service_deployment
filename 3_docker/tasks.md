# Deploy services as container

1. Our database shall run within a separate container. Therefore, we download version 13.0 of the official postgres image from dockerhub.
  - https://hub.docker.com/_/postgres

```bash
sudo docker pull postgres:13.0
```

2. The images is now included in the list of all locally available images.

```bash
sudo docker images
```

3. We use the postgres image to start a database container.
  - **-p 5432** publishes ports 5432 which is used by postgresql api
  - **-e XXX=XXX** sets environmental variables within container
  - **-d** runs container in background and print container ID

```bash
sudo docker run \
  -p 5432:5432 \
  -e POSTGRES_USER=my_user \
  -e POSTGRES_PASSWORD=password \
  -e POSTGRES_DB=sc_data \
  -d postgres:13.0 postgres
```

4. Lets check if container is running:

```bash
sudo docker ps
```

5. The database container is running within docker default bridge network. To access our database from another container, we need to note the gateway ip adress of the default bridge network.
  - "Gateway": "172.17.0.1"

```bash
sudo docker network inspect bridge
```

6. Next, we build an image containing conda with an r-shiny environment to initilize the database and to run the shiny app.

```bash
cd $HOME/3rd_denbi_user_meeting__service_deployment/3_docker/rshiny
sudo docker build -t rshiny:v1 .
```

7. We run an r-shiny container to initialize our database. Thereby we attach data and script folder as volumes into the container.
  e.g. `-v $WORKSPACE/3rd_denbi_user_meeting__service_deployment/scripts/:/scripts/`

```bash
sudo docker run \
  -e DB_USER="my_user"\
  -e DB_PASSWORD="password"\
  -e DB_NAME="sc_data"\
  -e DB_HOST="172.17.0.1"\
  -e DB_PORT="5432"\
  -v $HOME/3rd_denbi_user_meeting__service_deployment/scripts:/scripts \
  -v $HOME/3rd_denbi_user_meeting__service_deployment/data:/data \
  rshiny:v1 Rscript /scripts/init_db.R
```

8. After execution our r-shiny db_init container exits. Lets check, if the data was inserted correctly by running a new interactive container.
  - -it starts an interactive session

```bash
sudo docker run -it \
  -e DB_USER="my_user" \
  -e DB_PASSWORD="password" \
  -e DB_NAME="sc_data" \
  -e DB_HOST="172.17.0.1" \
  -e DB_PORT="5432" \
  rshiny:v1 R
```

```R
library("DBI")
library("tidyverse")
library("dbplyr")

get_connection <- function() {
    con <- dbConnect(
        RPostgreSQL::PostgreSQL(),
        dbname = Sys.getenv("DB_NAME"),
        host=Sys.getenv("DB_HOST"),
        port=Sys.getenv("DB_PORT"),
        user=Sys.getenv("DB_USER"),
        password=Sys.getenv("DB_PASSWORD"))
    return(con)
}

con <- get_connection()
meta <- tbl(con, "meta")
meta %>%
  head()
meta %>%
  collect() %>%
  dim()
counts <- tbl(con, "counts")
counts %>%
  head()
counts %>%
  summarise(n=n())
dbDisconnect(con)

q("no")
```

9. Finally, we can start our single cell explorer app in docker

```bash
sudo docker run \
  -e DB_USER="my_user" \
  -e DB_PASSWORD="password" \
  -e DB_NAME="sc_data" \
  -e DB_HOST="172.17.0.1" \
  -e DB_PORT="5432" \
  -e SHINY_APP="/scripts/app.R" \
  -e SHINY_PORT="8000" \
  -p 8000:8000 \
  -v $HOME/3rd_denbi_user_meeting__service_deployment/scripts:/scripts \
  -v $HOME/3rd_denbi_user_meeting__service_deployment/data:/data \
  rshiny:v1 Rscript /scripts/start_shiny.R
```

